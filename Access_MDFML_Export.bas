Attribute VB_Name = "Access_MDFML_Export"
Option Compare Database
Option Explicit

' Edited: 2022-01-13
' Single Module to Export an Access Application to MDFML files for rebuilding via custom system
Private Const SelfDoc_Module = "Access_MDFML_Export"

' MDFML Export
' MDFML = MetaDataForm Markup Language, a use of XML

Private Const TagStr_MDFML = "mdfml"

Private Const TagStr_window = "window" ' indepedent display window, usually employing a quad
Private Const TagStr_defquad = "defquad" ' a named container - used for populating a window or a subform
Private Const TagStr_usequad = "usequad" ' window attribute to point to a quad

Private Const TagStr_datatray = "datatray" ' a holder of a dataform and/or datasheet
Private Const TagStr_dataform = "dataform" '  a form or subform, an interactive
Private Const TagStr_datasheet = "datasheet" ' grid of a dataset fetched via a dataref

Private Const TagStr_one_uid = "one_uid" '  identifying name, must be unique in its context
' data attributes
Private Const TagStr_defdataref = "defdataref" '  definition of a data query reference, a named element
Private Const TagStr_rstref = "rstref"
Private Const TagStr_usedataref = "usedataref" '  definition of a data query reference, a named element
'
Private Const TagStr_one_name = "one_name" '  identifying name, must be unique in its context
'form_mode = "form_mode" '  form|sheet
'one_default_dataref = "one_default_dataref" '  which dataref name is the default/initial one
'one_default_form_mode = "one_default_form_mode" '  which form_mode is the default/initial one
' the simple controls
Private Const TagStr_ctrl_label = "ctrl_label" '  a label control
Private Const TagStr_ctrl_textbox = "ctrl_textbox" '  a textbox control
Private Const TagStr_ctrl_listbox = "ctrl_listbox" '  a listbox control
Private Const TagStr_ctrl_combobox = "ctrl_combobox" '  a combobox control
Private Const TagStr_ctrl_checkbox = "ctrl_checkbox" '  a checkbox control
Private Const TagStr_ctrl_button = "ctrl_button" '  a button control
' tab/page controls
Private Const TagStr_ctrl_tabset = "ctrl_tabset" '
Private Const TagStr_ctrl_subtab = "ctrl_subtab" '
' control attributes
Private Const TagStr_one_caption = "one_caption" '   a display text for something
' layout directives
Private Const TagStr_subdiv = "subdiv" '  subdivision, frame
'one_ctrl = "one_ctrl" '  for a label, the name of its associated control
'one_datacol = "one_datacol" '  the data column supplying/supplied-by the control
'one_datacol_name = "one_datacol_name" '  the data column providing the controls selected value
'see_datacol_name = "see_datacol_name" '  a data column to be displayed
'one_click_actiontype = "one_click_actiontype" '  values:(form_action_exit|etc)
'one_click_actionref = "one_click_actionref" '  reference to code element
'one_uform_datalink = "one_uform_datalink" '  definitinon of subform-to-form data relation
'link_pair = "link_pair" '  two lists of joining columns
'one_busform_datacol = "one_busform_datacol" '  list of joining colums in the busform
'one_subform_datacol = "one_subform_datacol" '  list of joining colums in the subform
Private Const TagStr_place_x = "place_x" '  x position absolute from top left
Private Const TagStr_place_y = "place_y" '  y position absolute from top left
Private Const TagStr_place_w = "place_w" '  width
Private Const TagStr_place_h = "place_h" '  height

' -------------------------------
' String Line building call

Private Sub StrAddLin(ByRef s As String, ByVal a As String)
If Len(s) > 0 Then
  s = s & vbCrLf & a
Else
  s = a
  End If
End Sub

Private Function Xmlify(ByVal s As String) As String
Dim r As String
r = replace(s, "&", "&amp;")
r = replace(r, "<", "&lt;")
r = replace(r, ">", "&gt;")
r = replace(r, "'", "&apos;")
r = replace(r, """", "&quot;")
Xmlify = r
End Function

' -------------------------------
' Tag assembly functions

Private Function TagOf_Endin(ByVal ts As String)
TagOf_Endin = "</" & Xmlify(ts) & ">"
End Function

Private Function TagOf_Begin(ByVal ts As String)
TagOf_Begin = "<" & Xmlify(ts) & ">"
End Function

Private Function TagOf_Begin_Value_Endin(ByVal ts As String, ByVal valu As String)
TagOf_Begin_Value_Endin = TagOf_Begin(ts) & Xmlify(valu) & TagOf_Endin(ts)
End Function

Private Function TagOf_Begin_ValueNum_Endin(ByVal ts As String, ByVal valu As Integer)
TagOf_Begin_ValueNum_Endin = TagOf_Begin(ts) & Xmlify(Trim(str(valu))) & TagOf_Endin(ts)
End Function

Private Function TagOf_Begin_ValueNumPoints_Endin(ByVal ts As String, ByVal valuePoints As Long)
Dim valuePixels As Long
valuePixels = CInt(valuePoints / 15)
'valuePixels = Application.ActiveWindow.PointsToScreenPixelsX(valuePoints)
TagOf_Begin_ValueNumPoints_Endin = TagOf_Begin(ts) & Xmlify(Trim(str(valuePixels))) & TagOf_Endin(ts)
End Function

'
' -------------------------------

Private Function QuadNameFromFormName(ByVal fn As String) As String
' this needs to be a consistent way of making a name exclusive for the DefQuad and UseQuad tags
' this will be used in two places:
' - when defining a quad
' - when referring to the quad - instead of the form name i.e. when using as a subform
QuadNameFromFormName = "Dauq_" & fn
End Function


' -------------------------------
' All the various controls - in alphabetic order

Private Function MDF_Make_MDFML_For_Control_CheckBox(ByRef ctrl As CheckBox, ByVal VerticalOffset As Long) As String
Dim s As String
Debug.Print "MDF_Make_MDFML_For_Control_CheckBox", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_ctrl_checkbox))
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_ctrl_checkbox))
MDF_Make_MDFML_For_Control_CheckBox = s
End Function

Private Function MDF_Make_MDFML_For_Control_ComboBox(ByRef ctrl As ComboBox, ByVal VerticalOffset As Long) As String
Dim s As String
Debug.Print "MDF_Make_MDFML_For_Control_ComboBox", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_ctrl_combobox))
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  '!! still to do:
  Debug.Print .BoundColumn
  Debug.Print .ColumnCount
  Debug.Print .ControlSource
  Debug.Print .RowSource
  Debug.Print .RowSourceType
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_ctrl_combobox))
MDF_Make_MDFML_For_Control_ComboBox = s
End Function

Private Function MDF_Make_MDFML_For_Control_CommandButton(ByRef ctrl As CommandButton, ByVal VerticalOffset As Long) As String
Dim s As String
Debug.Print "MDF_Make_MDFML_For_Control_CommandButton", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_ctrl_button))
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_caption, .Caption))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  '!! still to do:
  Debug.Print .OnClick
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_ctrl_button))
MDF_Make_MDFML_For_Control_CommandButton = s
End Function

Private Function MDF_Make_MDFML_For_Control_Label(ByRef ctrl As Label, ByVal VerticalOffset As Long) As String
Dim s As String
Debug.Print "MDF_Make_MDFML_For_Control_Label", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_ctrl_label))
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_caption, .Caption))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_ctrl_label))
MDF_Make_MDFML_For_Control_Label = s
End Function

Private Function MDF_Make_MDFML_For_Control_ListBox(ByRef ctrl As ListBox, ByVal VerticalOffset As Long) As String
Dim s As String
Debug.Print "MDF_Make_MDFML_For_Control_ListBox", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_ctrl_listbox))
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  '!! still to do:
  Debug.Print
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_ctrl_listbox))
MDF_Make_MDFML_For_Control_ListBox = s
End Function

Private Function MDF_Make_MDFML_For_Control_OptionButton(ByRef ctrl As OptionButton, ByVal VerticalOffset As Long) As String
Dim s As String
Debug.Print "MDF_Make_MDFML_For_Control_OptionButton", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_ctrl_checkbox)) ' treat as a checkbox for now
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  '!! still to do:
  Debug.Print
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_ctrl_checkbox))
MDF_Make_MDFML_For_Control_OptionButton = s
End Function

Private Function MDF_Make_MDFML_For_Control_OptionGroup(ByRef ctrl As OptionGroup, ByVal VerticalOffset As Long) As String
Dim s As String
Debug.Print "MDF_Make_MDFML_For_Control_OptionGroup", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_subdiv)) ' treat as a checkbox for now
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  '!! still to do:
  Debug.Print .Controls.Count
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_subdiv))
MDF_Make_MDFML_For_Control_OptionGroup = s
End Function

Private Function MDF_Make_MDFML_For_Control_SubForm(ByRef ctrl As SubForm, ByVal VerticalOffset As Long) As String
Dim s As String
Debug.Print "MDF_Make_MDFML_For_Control_SubForm", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_subdiv)) '
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  '
  Call StrAddLin(s, TagOf_Begin(TagStr_usequad)) '
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, QuadNameFromFormName(.SourceObject)))
  '!! still to do:
  Debug.Print .LinkChildFields
  Debug.Print .LinkMasterFields
  Call StrAddLin(s, TagOf_Endin(TagStr_usequad))
  '
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_subdiv))
MDF_Make_MDFML_For_Control_SubForm = s
End Function

Private Function MDF_Make_MDFML_For_Control_Tab(ByRef ctrl As Control, ByVal VerticalOffset As Long) As String
Dim s As String
Dim t_s As String
Dim p_n As Integer
Dim i As Integer
Dim p_ctl As Page 'Control
Dim sbtg_ctrl As Control
Debug.Print "MDF_Make_MDFML_For_Control_Tab", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_ctrl_tabset))
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  p_n = ctrl.Pages.Count
  If p_n > 0 Then
    For i = 0 To p_n - 1
      Set p_ctl = ctrl.Pages(i)
      Call StrAddLin(s, TagOf_Begin(TagStr_ctrl_subtab))
      Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, p_ctl.name))
      Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_caption, p_ctl.Caption))
      For Each sbtg_ctrl In p_ctl.Controls
        t_s = MDF_MDFML_For_Each_Control(sbtg_ctrl, 0) ' ?? + VerticalOffset
        Call StrAddLin(s, t_s)
        Next sbtg_ctrl
      Call StrAddLin(s, TagOf_Endin(TagStr_ctrl_subtab))
      Next i
    End If
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_ctrl_tabset))
MDF_Make_MDFML_For_Control_Tab = s
End Function

Private Function MDF_Make_MDFML_For_Control_TextBox(ByRef ctrl As TextBox, ByVal VerticalOffset As Long) As String
Dim s As String
Debug.Print "MDF_Make_MDFML_For_Control_TextBox", ctrl.name
s = ""
Call StrAddLin(s, TagOf_Begin(TagStr_ctrl_textbox))
With ctrl
  Call StrAddLin(s, TagOf_Begin_Value_Endin(TagStr_one_name, .name))
  ' TagStr_place_x TagStr_place_y TagStr_place_w
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_x, .Left))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_y, .Top + VerticalOffset))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_w, .Width))
  Call StrAddLin(s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, .Height))
  Debug.Print .Section
  End With
Call StrAddLin(s, TagOf_Endin(TagStr_ctrl_textbox))
MDF_Make_MDFML_For_Control_TextBox = s
End Function



' -------------------------------
' Handle the Forms

Private Function MDF_MDFML_For_Each_Control(ByRef ctl As Control, ByVal VerticalOffset As Long) As String
Dim c_s As String
c_s = ""
Select Case ctl.ControlType
  Case acBoundObjectFrame  'Bound object frame
  Case acCheckBox  'Check box
    c_s = MDF_Make_MDFML_For_Control_CheckBox(ctl, VerticalOffset)
  Case acComboBox  'Combo box
    c_s = MDF_Make_MDFML_For_Control_ComboBox(ctl, VerticalOffset)
  Case acCommandButton   'Command button
    c_s = MDF_Make_MDFML_For_Control_CommandButton(ctl, VerticalOffset)
  Case acCustomControl   'ActiveX (custom) control
    Debug.Print "NOT YET: ActiveX (custom) control"
  Case acImage 'Image
    Debug.Print "NOT YET: Image"
  Case acLabel 'Label
    c_s = MDF_Make_MDFML_For_Control_Label(ctl, VerticalOffset)
  Case acLine 'line
    Debug.Print "NOT YET: Line"
  Case acListBox   'List box
    c_s = MDF_Make_MDFML_For_Control_ListBox(ctl, VerticalOffset)
  Case acObjectFrame   'Unbound object frame or chart
    Debug.Print "NOT YET: Unbound object frame or chart"
  Case acOptionButton  'Option button
    c_s = MDF_Make_MDFML_For_Control_OptionButton(ctl, VerticalOffset)
  Case acOptionGroup   'Option group - treat as a Subdiv
    c_s = MDF_Make_MDFML_For_Control_OptionGroup(ctl, VerticalOffset)
  Case acPage 'Page ' shouldn't need to detect this as the Tab control routine should directly loop through them
    Debug.Print "NOT YET: Page"
'    Case acPagectrl 'Page Control OBSOLETE MAYBE?
'      Debug.Print "NOT YET: Page Control"
  Case acPageBreak   'Page break
    Debug.Print "NOT YET: Page break"
  Case acRectangle 'Rectangle
    Debug.Print "NOT YET: Rectangle"
  Case acSubform 'SubForm / SubReport
    Debug.Print "NOT YET: SubForm"
    c_s = MDF_Make_MDFML_For_Control_SubForm(ctl, VerticalOffset)
  Case acTabCtl  'Tab ' see PageCtrl
    c_s = MDF_Make_MDFML_For_Control_Tab(ctl, VerticalOffset)
  Case acTextBox   'Text box
    c_s = MDF_Make_MDFML_For_Control_TextBox(ctl, VerticalOffset)
  Case acToggleButton  ' Toggle button
    Debug.Print "NOT YET: Toggle button"
  Case Else
    Debug.Print "Unknown Control Type", ctl.ControlType
  End Select
MDF_MDFML_For_Each_Control = c_s
End Function

' For forms and reports, the Section property is an array of all existing sections in the form
' specified by the section number. For example, Section(0) refers to a form's detail section,
' and Section(3) refers to a form's page header section.
'Setting   Constant  Description
'0   acDetail  Form detail section
'1   acHeader  Form header section
'2   acFooter  Form footer section
'3   acPageHeader  Form page header section
'4   acPageFooter  Form page footer section

Private Sub Form_Has_Sections(ByRef frm As Form, _
  ByRef HasSection_Header As Boolean, ByRef HasSection_Detail As Boolean, ByRef HasSection_Footer As Boolean)
Dim ctl As Control
HasSection_Header = False
HasSection_Detail = False
HasSection_Footer = False
For Each ctl In frm
  If HasSection_Header = False And ctl.Section = acHeader Then
    HasSection_Header = True
    End If
  If HasSection_Detail = False And ctl.Section = acDetail Then
    HasSection_Detail = True
    End If
  If HasSection_Footer = False And ctl.Section = acFooter Then
    HasSection_Footer = True
    End If
  Next ctl
End Sub

Private Function MDF_MDFML_For_FormObject_Controls(ByRef frm As Form, ByRef didok As Boolean) As String
Debug.Print "MDF_MDFML_For_FormObject_Controls", frm.name
Dim f_s As String
Dim c_s As String
Dim ctl As Control
Dim HasSection_Header As Boolean
Dim HasSection_Detail As Boolean
Dim HasSection_Footer As Boolean
Dim VerticalOffset As Long
didok = True
VerticalOffset = 0
f_s = ""
Call Form_Has_Sections(frm, HasSection_Header, HasSection_Detail, HasSection_Footer)
If HasSection_Header Then
  'Call StrAddLin(f_s, TagOf_Begin(TagStr_subdiv))
  'Call StrAddLin(f_s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, frm.Section(acHeader).Height))
  For Each ctl In frm.Section(acHeader).Controls
    c_s = MDF_MDFML_For_Each_Control(ctl, VerticalOffset)
    If Len(c_s) > 0 Then
      Call StrAddLin(f_s, c_s)
      End If
    Next ctl
  Set ctl = Nothing
  VerticalOffset = VerticalOffset + frm.Section(acHeader).Height
  'Call StrAddLin(f_s, TagOf_Endin(TagStr_subdiv)) '
  End If
If HasSection_Detail Then
  'Call StrAddLin(f_s, TagOf_Begin(TagStr_subdiv)) '
  'Call StrAddLin(f_s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, frm.Section(acDetail).Height))
  For Each ctl In frm.Section(acDetail).Controls
    c_s = MDF_MDFML_For_Each_Control(ctl, VerticalOffset)
    If Len(c_s) > 0 Then
      Call StrAddLin(f_s, c_s)
      End If
    Next ctl
  Set ctl = Nothing
  VerticalOffset = VerticalOffset + frm.Section(acDetail).Height
  'Call StrAddLin(f_s, TagOf_Endin(TagStr_subdiv)) '
  End If
If HasSection_Footer Then
  'Call StrAddLin(f_s, TagOf_Begin(TagStr_subdiv)) '
  'Call StrAddLin(f_s, TagOf_Begin_ValueNumPoints_Endin(TagStr_place_h, frm.Section(acFooter).Height))
  For Each ctl In frm.Section(acFooter).Controls
    c_s = MDF_MDFML_For_Each_Control(ctl, VerticalOffset)
    If Len(c_s) > 0 Then
      Call StrAddLin(f_s, c_s)
      End If
    Next ctl
  VerticalOffset = VerticalOffset + frm.Section(acFooter).Height
  Set ctl = Nothing
  'Call StrAddLin(f_s, TagOf_Endin(TagStr_subdiv)) '
  End If
MDF_MDFML_For_FormObject_Controls = f_s
Debug.Print "Form Height = total VerticalOffset", VerticalOffset
End Function

Private Function MDF_MDFML_For_FormObject(ByRef frm As Form, ByRef didok As Boolean) As String
Debug.Print "MDF_MDFML_For_FormObject", frm.name
Dim f_s As String
Dim f_c_s As String
Dim quadname As String
quadname = QuadNameFromFormName(frm.name)
f_s = ""
' put a dataform inside a datatray inside a defquad
Call StrAddLin(f_s, TagOf_Begin(TagStr_defquad))
Call StrAddLin(f_s, TagOf_Begin_Value_Endin(TagStr_one_uid, quadname))

Call StrAddLin(f_s, TagOf_Begin(TagStr_datatray))
Call StrAddLin(f_s, TagOf_Begin(TagStr_dataform))
' now for the controls inside the form
f_c_s = MDF_MDFML_For_FormObject_Controls(frm, didok)
Call StrAddLin(f_s, f_c_s)
Call StrAddLin(f_s, TagOf_Endin(TagStr_dataform))
'Call StrAddLin(f_s, TagOf_Begin(TagStr_datasheet))
'Call StrAddLin(f_s, TagOf_Endin(TagStr_datasheet))
Call StrAddLin(f_s, TagOf_Endin(TagStr_datatray))
Call StrAddLin(f_s, TagOf_Endin(TagStr_defquad))
'
' now make a window that uses the defquad
' this represents the form opened as itself
Call StrAddLin(f_s, TagOf_Begin(TagStr_window))
Call StrAddLin(f_s, TagOf_Begin_Value_Endin(TagStr_one_name, frm.name))
Call StrAddLin(f_s, TagOf_Begin_Value_Endin(TagStr_usequad, quadname))
Call StrAddLin(f_s, TagOf_Endin(TagStr_window))
'
MDF_MDFML_For_FormObject = f_s
End Function

Private Sub WriteStringAsFile(ByRef s As String, ByVal FileName As String)
Dim x As Long
x = FreeFile
'On Error GoTo file_bad
Open FileName For Output As #x
Print #x, s
Close #x
file_bad:
On Error GoTo 0
End Sub

' Loop through all the Forms
' The method is:
' - use the AllForms collection to get all named forms
' - open each form for design
' - use the Forms collection to get a Form object
' - analyse the form
' - close the form

Public Sub MDF_Export_AllForms_To_MDFML(ByVal output_name As String)
Dim obj As AccessObject
Dim dbs As Object
Dim frm As Form
'
Dim didok As Boolean
Dim s As String
Dim m_s As String
Dim ao As AccessObject
Dim f_c_s As String
Dim i As Integer
Debug.Print "MDF_Export_AllForms_To_MDFML"
Set dbs = Application.CurrentProject
s = ""
For Each obj In dbs.AllForms
  If Not obj.IsLoaded = True Then
    DoCmd.OpenForm obj.name, acDesign
    End If
  If obj.IsLoaded = True Then
    Debug.Print obj.name
    Set frm = Forms(obj.name)
    f_c_s = MDF_MDFML_For_FormObject(frm, didok)
    Debug.Print f_c_s
    DoCmd.Close acForm, obj.name
    Call StrAddLin(s, f_c_s)
    End If
  Next obj
If Len(s) > 0 Then
  m_s = ""
  Call StrAddLin(m_s, TagOf_Begin(TagStr_MDFML))
  Call StrAddLin(m_s, s)
  Call StrAddLin(m_s, TagOf_Endin(TagStr_MDFML))
  Call WriteStringAsFile(m_s, output_name)
  End If
Debug.Print "MDF_Export_AllForms_To_MDFML", "Completed!"
End Sub

'############################################################################
' Example test calls

' MDF_Export_AllForms_To_MDFML( "J:\VCR_MDFML.xml" )
