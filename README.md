# MetaDataForm VbaAccess

This is a VBA module for Microsoft Access as part of the wider concept of MetaDataForm, a process and tools based around a meta-language for defining data based interfacing. See elsewhere for information about the wider project. See [MetaDataFormTkinter](https://gitlab.com/geraldew/metadataformtkinter)

The purpose of this module is to be something that can be imported into an Access database application and then executed to export an MDFML definition of the application's Forms. The output therefore is an XML file using the MDFML (MetaDataForm Markup Language) structures.

That output would then be used by one of the receiving-end MetaDataForm tools. At time of writing, the first of these (for live Tkinter rendering in Python) is being simultaneously developed.

## Current Status

- all forms are processed to some degree
- most control types are handled
- most sizing and positioning is handled

Some features are waiting until there is a suitable way of creating a suitable data structures for the target systems. While that's a dependency it will not be tackled in this module.

The output is not currently as correct UTF-8 so some other tool is needed to convert it before testing with the target interface generator.

Currently, to use the module:
- download it
- import it via the VBA IDE inside the Access database
- at the very end of the module, you'll find a comment with the one-identifier string to paste into the Immediate window and press ENTER.
- it will whiz through all the forms - there will be a visual blur of forms opening and closing durring this process
- it will generate an output text file - there is a single point where that (path and file name) is specified

# Addtional Notes

(being somewhat randomly added until structure becomes apparent)

- as per the deliberate design of MDFML, the Forms as single items get replicated into two items. Their use as "main" forms becomes "windows" quoting themselves as "quads". Where the forms are used as sub-forms, it is the quads that are quoted. To handle this, there is an automated extra name - currently using the reverse of "quad" - i.e. "dauq" as a prefix.

- some obcure control types are currently ignored completely

- "option buttons" (aka "radio buttons") are not yet properly handled. It hasn't yet been decided whether to honour the concept of just convert them all into combo boxes. As Option Group controls are part of the same concept then they are also not yet properly handled.

- by default, the text/string handling inside Access & VBA is done with a strange Microsoft version of Unicode. This presents two problems.
    - the first is that the generated XML output is not in UTF-8. It is likely that output-specific code will be brought in to deal with that.
    - the second is that some symbols/characters are simply _wrong_ - and that is likely to remain the same until almost all major matters are dealt with

- Access forms allow for shortcut keys to be set by placing ampersand symbols in some control values.

- testing of how the transformed Form interface operates will be dependent on having equivalent data available in the target - so for now, all that is likely to be done is put relevant symbols in place to allow for that to be done manually.

- currently Headers and Footers in forms are handled by merely noting their height and using that to add vertical offsets to the position values for the controls therein. i.e. MDFML currently doesn't support a concept of headers and footers. However, the controls in all three sections are processed.
- however, any Form Sections of PageHeader and PageFooter are just ignored - as are the controls within them
- the Access Form object model is quite clumsy about Sections, so currently the method to determine which sections exist is to run through all the controls on a Form and note which Sections were quoted in the Control's "Section" value. Attempts to handle this via error trapping just didn't work! Thus a Section with no controls is currently treated as not existing at all.

- currently the entire XML build is done by building a string variable and then writing that out to a text file. It doesn't seem likely that this will be inadequate for the vast majority of situations.

## Test application

The public test being used is the Northwind sample database from Access 2003.

## Software License

This is Free Open Source Software (FOSS) using the GPL3 license.

As a note to those who may find that concerning, let me be clear:
- the author has followed the evolution of FOSS since its birth and is well versed in the concepts;
- Yes, I do know that GPL is not the norm for Python applications;
- Copyright - and thus the license - applies to my specific code, so a fork of this source code has to also respect and apply the GPL;
- FOSS means you are free to study the code and use what you learn to write your own software, where you can thereby choose a license. No need to ask, please go ahead and do that.

## Addtional License Remarks

One of the oddities of the MS Access and VBA worlds is that there are:
- many, many openly shared code snippets (a la Stack Overflow) with essentially no copyright statements (*);
- many places where the Microsoft documentation provides example code for manipulating the Access object model - i.e. such that documentation IS code;
- often, due to the Access object model there really can be only one way to do something.

So this does mean that parts of the code in this module may have exact matches for code published online. Noting therefore that copyright has force even if no apparent license is present (*), I attest that this code module only coincides by accident (albeit, such being inevitable intersections).

